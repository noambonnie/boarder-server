const config = require('config');
const fcm = require('node-gcm'); // GCM is now called FCM. Library name remains gcm but otherwise will use FCM.
const logger = require('winston');

logger.info("Creating FCM provider");
let fcmProvider = new fcm.Sender(config.get('thirdparty.gcm.apiKey'));

const messageSender = {
    android: (clients, type, message, title, body) => {
        switch (type) {
            case 'silent': {
                    const fcmMessage = new fcm.Message({
                        priority: 'high',
                        contentAvailable: true,
                        delayWhileIdle: false,
                        data: {
                            // title,
                            // message: body,
                            boarderData: JSON.stringify(message),
                        },
                    });

                    fcmProvider.send(fcmMessage, clients, (err, res) => {
                        err && logger.error('Error sending FCM notification', err);
                    });
                }
                break;
            case 'noisy': {
                    const fcmMessage = new fcm.Message({
                        priority: 'high',
                        contentAvailable: false,
                        delayWhileIdle: false,
                        notification: {
                            title,
                            body,
                            boarderData: JSON.stringify(message),
                        },
                    });
                    fcmProvider.send(fcmMessage, clients, (err, res) => {
                        err && logger.error('Error sending FCM notification', err);
                    });
                }
                break;
        }
    }
}

module.exports.notifyClients = (clients, type, message, title, body) => {
    const groupedClientIds = {
        android: [],
        ios: [],
    }

    clients.map((client) => {
        client.os && client.os.name && (groupedClientIds[client.os.name].push(client.notificationToken));
    })

    // Currently keeping separation because I may move to sending directly to APNs. I need more research time.
    // But using 'android' sender (which is FCM which supports APNs).
    if (groupedClientIds['android'].length > 0)
        messageSender['android'](groupedClientIds['android'], type, message, title, body)

    if (groupedClientIds['ios'].length > 0)
        messageSender['android'](groupedClientIds['ios'], type, message, title, body)
}
