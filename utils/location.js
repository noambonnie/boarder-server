module.exports.toGeoJSONPoint = (long, lat) => {
    return {
        type: "Point",
        coordinates: [long, lat],
    }

}