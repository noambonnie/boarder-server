const mongoose = require('mongoose');
const logger = require('winston');
const config = require('config');
const passportLocalMongoose = require('passport-local-mongoose');

const Schema = mongoose.Schema;

let UserSchema = new Schema({
    firstName: { type: String },
    lastName: { type: String },
    email: { type: String, required: true, lowercase: true, trim: true, index: { unique: true } },
    languages: { type: [String] },
    dateOfBirth: { type: Date },
    dateCreated: { type: Date, default: Date.now },
    lastActive: { type: Date, default: Date.now },
    // For reset we use a reset token with an expiry (which must be checked)
    resetToken: { type: String },
    resetTokenExpiresMillis: { type: Number },
    profilePic: { type: String },
    blockedUsers: { type: [mongoose.Schema.Types.ObjectId] }, // Users I blocked
    blockedBy: { type: [mongoose.Schema.Types.ObjectId] }, // Users who blocked me.
    googleId: { type: String },
    facebookId: { type: String },
});

// Let passport local mongoose handle the creation of users with hashing salting and stuff.
// We'll use this passport strategy instead of the pure LocalStrategy to authenticate. 
UserSchema.plugin(passportLocalMongoose, {
    usernameField: 'email',
    usernameUnique: true,
    usernameLowerCase: true,
    saltField: 'pwSalt',
    hashField: 'pwHash',
    attemptsField: 'loginFailureAttempts',
    lastLoginField: 'lastLogin',
});


UserSchema.statics.findUserByEmail = function (email, callback) {

    this.findOne({ email: email }, function (err, user) {
        if (err || !user) {
            callback(err, null);
        }
        else {
            callback(false, user);
        }
    });
};

UserSchema.statics.generateResetToken = function (email, callback) {

    this.findUserByEmail(email, function (err, user) {
        if (err) {
            callback(err, null);
        }
        else if (user) {
            // Generate reset token and URL link; also, create expiry for reset token
            user.resetToken = require('crypto').randomBytes(32).toString('hex');
            var now = new Date();
            var expires = new Date(now.getTime() + (config.get('security.pwResetTokenExpiresMinutes') * 60 * 1000)).getTime();
            user.resetTokenExpiresMillis = expires;
            user.save();
            callback(false, user);
        }
        else {
            // TODO: This is not really robust and we should probably return an error code or something here
            callback(new Error('No user with that email found.'), null);
        }
    });
};

module.exports = mongoose.model('User', UserSchema);
