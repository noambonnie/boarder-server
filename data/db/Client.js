const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;

let ClientSchema = new mongoose.Schema({
    userId: { type: ObjectId, required: true },
    refreshToken: { type: String },
    issueDate: { type: Date, required: true, default: Date.now },
    os: { type: Object, required: true },
    "os.name": { type: String, required: true },
    "os.version": { type: String, required: true },
    notificationToken: { type: String },
    lastActive: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Client', ClientSchema);