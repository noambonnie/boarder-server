const mongoose = require('mongoose');

let GeoJSONSchema = new mongoose.Schema({
    'type': {
        type: String,
        required: true,
        enum: ['Point'],
        default: 'Point'
    },
    coordinates: [Number]
});

module.exports = GeoJSONSchema;