const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;

const NotificationSchema = new mongoose.Schema({
    userId: { type: ObjectId, required: true }, // Who the notification is for
    type: { type: String, required: true },
    modifiedAt: { type: Date, required: true, default: Date.now},
    referenceObject: { type: ObjectId }, // Reference to the conversation or post for this notification
    notificationCount: { type: Number, required: true, default: 1 },
    seen: { type: Boolean, required: true, default: false },
    read: { type: Boolean, required: true, default: false },
});

module.exports = mongoose.model('Notification', NotificationSchema);