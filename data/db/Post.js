const mongoose = require('mongoose');
const GeoJSON = require('./GeoJSONSchema');

const ObjectId = mongoose.Schema.Types.ObjectId;

const PostSchema = new mongoose.Schema({
    title: { type: String, },
    text: { type: String },
    postDate: { type: Date, required: true, default: Date.now },
    createdAt: { type: Date, required: true, default: Date.now },
    parentPostId: { type: ObjectId },
    geolocation: GeoJSON,
    city: { type: String },
    country: { type: String },
    postedBy: { type: ObjectId, ref: 'User', required: true, },
    images: { type: [String] }
});

module.exports = mongoose.model('Post', PostSchema);