const User = require('./db/User');

module.exports.updateUser = async (userId, newData) => {
    return User.findOneAndUpdate({ _id: userId }, newData, { runValidators: true, new: true }).lean().exec();
}

module.exports.getUserOwnData = (userId) => {
    return User.findById(userId)
        .select('firstName lastName profilePic email')
        .lean()
        .exec();
}

module.exports.getUserPublicData = (userId) => {
    return User.findById(userId)
        .select('firstName lastName profilePic')
        .lean()
        .exec();
}

module.exports.blockUser = (userId, blockedUserId) => {
    return User.update({ _id: userId }, { $addToSet: { blockedUsers: blockedUserId } }).exec();
}

module.exports.setBlockedBy = (userId, blockedUserId) => {
    return User.update({ _id: blockedUserId }, { $addToSet: { blockedBy: userId } }).exec();
}

module.exports.getUserQueryRestrictions = (userId) => {
    return User.findById(userId)
        .select('blockedUsers blockedBy')
        .lean()
        .exec();
}

module.exports.updateUserLastActivity = (userId) => {
    return User.update({ _id: userId }, { lastActive: Date.now() }).exec();
}

module.exports.findOrCreateGoogleUser = async (userInfo) => {
    // NBTODO: Currently a naive implementation - if the gmail address already exists, 
    //         we take over the old account with the new one.
    return User.findOneAndUpdate(
        {
            $or: [{ googleId: userInfo.sub }, { email: userInfo.email }]
        },
        {
            googleId: userInfo.sub,
            pwSalt: "",
            pwHash: "",
            $setOnInsert: {
                firstName: userInfo.given_name,
                lastName: userInfo.family_name,
                email: userInfo.email,
                profilePic: userInfo.picture,
            }
        },
        {
            upsert: true,
            setDefaultsOnInsert: true,
            new: true,
            runValidators: true,
        })
        .exec();
} 

module.exports.findOrCreateFacebookUser = async (userInfo) => {
    // NBTODO: Currently a naive implementation - if the gmail address already exists, 
    //         we take over the old account with the new one.
    return User.findOneAndUpdate(
        {
            $or: [{ facebookId: userInfo.id }, { email: userInfo.email }]
        },
        {
            facebookId: userInfo.id,
            pwSalt: "",
            pwHash: "",
            $setOnInsert: {
                firstName: userInfo.first_name,
                lastName: userInfo.last_name,
                email: userInfo.email,
                profilePic: userInfo.picture.data.url,
            }
        },
        {
            upsert: true,
            setDefaultsOnInsert: true,
            new: true,
            runValidators: true,
        })
        .exec();
} 