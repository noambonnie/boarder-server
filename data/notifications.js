const Notification = require('./db/Notification');
const Post = require('./db/Post');

module.exports.addNotification = async (userId, type, referenceObject) => {
    return Notification.findOneAndUpdate({}, { userId, type, referenceObject, modifiedAt: Date.now(), $inc: { notificationCount: 1 } }, { upsert: true, setDefaultsOnInsert: true })
                .where('userId').equals(userId)
                .where('type').equals(type)
                .where('referenceObject').equals(referenceObject)
                .where('seen').equals(false)
                .exec()
}

module.exports.getNotificationsCount = async (userId) => {
    return Notification.find({})
                       .where('userId').equals(userId)
                       .where('seen').equals(false)
                       .count()
                       .exec();
}

module.exports.getNotifications = async (userId, page, items, startDate) => {
    page = page || 1;
    items = (!items || items > 50) ? 50 : items; // NBTODO: Configuration. Limit number of results per page.
    startDate = startDate ? new Date(startDate) : new Date();

    return Notification.find()
        .select("_id referenceObject read type notificationCount modifiedAt")
        .where("userId").equals(userId)
        .where("modifiedAt").lt(startDate)
        .sort({ modifiedAt: -1 })
        .skip((page - 1) * items)
        .limit(Number(items))
        .lean() // We don't need the mongoose methods here
        .exec()
        .then((results) => {

            // Fetch comment counts for all posts. Create an array of promises to wait on.
            // Also calculate the distance of each post (and remove the precise location).
            const populatedNotifications = results.map((notification) => {
                switch(notification.type) {
                    case 'postComment': 
                        return Post.findById(notification.referenceObject)
                                   .select("_id title")
                                   .lean()
                                   .exec()
                            .then((referenceObject) => ({ ...notification, referenceObject }));
                        break;
                    default:
                        break;
                }
            })

            return Promise.all(populatedNotifications);
        });
}

module.exports.markRead = async (userId, notificationId) => {
    // Not using findByIdAndUpdate because that would allow a user to update another user's notification.
    // NBTODO: Need a better way of protecting this sort of things. Might even be better to findById and then
    //         updateOneById because the current query scans the entire collection.
    // NBTODO: I believe the above statement is false regarding the full collection scan. 
    //         And also, I don't think I need a find here. Update is good enough. 
    return Notification.findOneAndUpdate({}, { read: true })
                .where('_id').equals(notificationId)
                .where('userId').equals(userId)
                .exec()
}

module.exports.markAllSeen = async (userId) => {
    return Notification.updateMany({}, { seen: true })
                .where('userId').equals(userId)
                .where('seen').equals(false)
                .exec()
}
