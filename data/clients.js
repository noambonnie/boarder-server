const Client = require('./db/Client');

module.exports.createClient = async (userId, clientInfo) => {

    return await Client.create(
        { 
            userId: userId, 
            os: clientInfo.os, 
            notificationToken: clientInfo.notificationToken 
        });
}

module.exports.setNotificationToken = async (clientId, notificationToken) => {
    return await Client.findByIdAndUpdate(clientId, { notificationToken });
}

module.exports.setRefreshToken = async (clientId, refreshToken) => {
    return await Client.findByIdAndUpdate(clientId, { refreshToken: refreshToken, issueDate: Date.now() });
}

module.exports.findById = async (clientId) => {
    return await Client.findById({ _id: clientId });
}

module.exports.removeById = async (clientId) => {
    return await Client.remove({ _id: clientId })
}

module.exports.removeClientsByNotificationToken = async (notificationToken) => {
    notificationToken && await Client.remove({notificationToken});
}

module.exports.getUserClients = async (userId) => {
    return await Client.find({userId}).lean().exec();
}

module.exports.updateClientLastActivity = (clientId) => {
    return Client.update({ _id: clientId }, { lastActive: Date.now() }).exec();
}