const Post = require('./db/Post');
const locationUtils = require('../utils/location');
const geojsonUtils = require('geojson-utils');

module.exports.getFeed = (filter, page, items, startDate, queryRestrictions) => {
    page = page || 1;
    items = (!items || items > 50) ? 50 : items; // NBTODO: Configuration. Limit number of results per page.
    startDate = startDate ? new Date(startDate) : new Date();
    queryLocation = locationUtils.toGeoJSONPoint(filter.byDistance.position.longitude, filter.byDistance.position.latitude);

    // Build the feed query. TODO: Think about caching, sharding...
    let query = Post.find();
    if (filter.byDistance) {
        query.where("geolocation").near(
            {
                center: queryLocation,
                maxDistance: filter.byDistance.distance,
                spherical: true
            });
    }
    else if (filter.byCity) {
        res.send(404); // Unsupported for now until I integrate a reverse mapping API. Needs some research.
        // See http://www.geonames.org
        query.where("city")
    }

    // Hide all users I blocked or have been blocked by.
    const blockedUsers = [
            ...(queryRestrictions && queryRestrictions.blockedUsers || []),
            ...(queryRestrictions && queryRestrictions.blockedBy || [])
        ];

    return query
        .select("title text postedBy geolocation postDate")
        .populate("postedBy", "_id firstName lastName profilePic")
        .where("postDate").lte(startDate).lte(Date.now())
        .where("parentPostId").equals(null)
        .where("postedBy").nin(blockedUsers)
        .sort({ postDate: -1 })
        .skip((page - 1) * items)
        .limit(Number(items))
        .lean() // We don't need the mongoose methods here
        .exec()
        .then((results) => {

            // Fetch comment counts for all posts. Create an array of promises to wait on.
            // Also calculate the distance of each post (and remove the precise location).
            const populatedPosts = results.map((post) => {
                return Post.count().where({ parentPostId: post._id }).exec().then((commentsCount) => {
                    return {
                        ...post,
                        commentsCount: commentsCount,
                        geolocation: undefined,
                        distance: Math.floor(geojsonUtils.pointDistance(queryLocation, post.geolocation) / 100) / 10,
                    };
                })
            })

            return Promise.all(populatedPosts);
        });

}
module.exports.getComments = (postId, queryRestrictions) => {
    return Post.find()
        .populate("postedBy", "_id firstName lastName profilePic")
        .where("parentPostId").equals(postId)
        .sort({ postDate: 1 })
        .lean()
        .exec();
}

module.exports.addComment = (parentPostId, userId, comment) => {
    let post = new Post();
    post.parentPostId = parentPostId;
    post.postedBy = userId;
    post.text = comment.text;

    return post.save();
}

module.exports.addPost = async (userId, postDetails) => {

    let post = new Post(postDetails);
    post.postedBy = userId;
    post.title = postDetails.title;
    post.text = postDetails.text;
    post.geolocation = locationUtils.toGeoJSONPoint(postDetails.geolocation.position.longitude,
        postDetails.geolocation.position.latitude);

    return (await post.save()).toJSON();
}

// NBTODO: A Weird function to have... Used to check that post exists. See if there's a better way.
module.exports.postCountById = async (postId) => {
    return Post.findById(postId).count().exec();
}

module.exports.getPostById = async (postId) => {
    return Post.findById(postId)
        .select("_id postedBy title text postDate images")  // Don't send specific location for safety reasons...
        .populate("postedBy", "_id firstName lastName profilePic")
        .lean()
        .exec();
}