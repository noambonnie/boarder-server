const userData = require('../data/users');

const config = require("config");
const request = require("request-promise");

const verify = async (token) => {
    return await request({
        uri: 'https://graph.facebook.com/me',
        qs: {
            fields: "id,first_name,last_name,picture.height(300),email",
            access_token: token
        },
        json: true // Automatically parses the JSON string in the response
    });

}

module.exports.loginOrRegister = async (fbAccessToken) => {
    const fbUserInfo = await verify(fbAccessToken);
    const userInfo = await userData.findOrCreateFacebookUser(fbUserInfo);
    const user = {
        id: userInfo.id,
        firstName: userInfo.firstName,
        lastName: userInfo.lastName,
        email: userInfo.email,
        profilePic: userInfo.profilePic,
    }

    return user;
}