const notificationData = require("../data/notifications");
const postData = require('../data/posts');
const userData = require('../data/users');
const clientData = require('../data/clients');
const logger = require('winston');
const pushNotificationsHandler = require('../utils/pushNotificationsHandler');

module.exports.addPostCommentNotification = async (userId, postId, comments, commentText) => {

    // Get a distinct list of commenters. 
    const commenters = new Set();
    comments.reduce((acc, comment) => {
        const commenterId = comment.postedBy._id.toString();
        acc.add(commenterId);
        return acc;
    }, commenters);

    // Up there we added the commenting user - s/he doesn't need a notification.
    commenters.delete(userId);

    try {
        // Notify the owner of the post as well...
        const post = await postData.getPostById(postId);
        if (post.postedBy._id.toString() !== userId) {
            commenters.add(post.postedBy._id.toString());
        }

        const commenterDetails = await userData.getUserPublicData(userId);

        commenters.forEach(async (commenterId) => {
            try {
                notificationData.addNotification(commenterId, 'postComment', postId);
                const userClients = await clientData.getUserClients(commenterId); 
                pushNotificationsHandler.notifyClients(
                    userClients, 
                    'silent', 
                    { type: 'postComment', commenter: commenterDetails, post, commentText },
                    `${commenterDetails.firstName} commented on ${post.title}`,
                    commentText,
                )
            }
            catch(error) {
                logger.error("Failed to send notifications to clients: ", error);
            }
        });
    }
    catch(error) {
        logger.error("Failed to add notifications: ", error);
    }
}

module.exports.markRead = async (userId, notificationId) => {
    return notificationData.markRead(userId, notificationId);
}

module.exports.markAllSeen = async (userId) => {
    return notificationData.markAllSeen(userId);
}