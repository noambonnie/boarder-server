const userData = require('../data/users');

const {OAuth2Client} = require('google-auth-library');
const config = require("config");
const googleKeys = require(process.cwd() + "/" + config.get("security.google.keysFileName"))

const client = new OAuth2Client(
    googleKeys.web.client_id,
);

async function verify(token) {
  const loginTicket = await client.verifyIdToken({
      idToken: token,
      audience:  [googleKeys.web.client_id, googleKeys.ios.client_id],  
  });

  return loginTicket.getPayload();
}

module.exports.loginOrRegister = async (googleIdToken) => {
    const googleUserInfo = await verify(googleIdToken);
    const userInfo = await userData.findOrCreateGoogleUser(googleUserInfo);
    const user = {
        id: userInfo.id,
        firstName: userInfo.firstName,
        lastName: userInfo.lastName,
        profilePic: userInfo.profilePic,
        email: userInfo.email,
    }
    
    return user;
}