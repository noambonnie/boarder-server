'use strict'
const mongoose = require('mongoose');
const config = require('config');
const logger = require('winston');
const express = require('express')
const app = express();
const https = require('https');
const http = require('http');
const bodyParser = require('body-parser');
const passport = require('passport');
const User = require('./data/db/User');
const fs = require('fs');
const aws = require('aws-sdk');

logger.level = config.get('logging.level');

aws.config.update({ 
    accessKeyId: config.get("thirdparty.aws.key"), 
    secretAccessKey: config.get("thirdparty.aws.secretKey") });

mongoose.Promise = require('bluebird'); // Mongoose uses promises internally and requires a promise library
                                        // NBTODO: Is it still needed with the version of Node that supports promises?

mongoose.connect(config.get("mongo.connectionString"), 
    { 
        useMongoClient: true,
        reconnectTries: Number.MAX_VALUE, // Never give up on retrying. Once given up, the connection cannot
                                          // be used anymore. I couldn't find a reliable way to know that
                                          // the connection is destroyed. Events don't seem to be triggered.
                                          // Consider further investigation.
        reconnectInterval: 1000,          // Sets the delay between every retry (milliseconds)
        keepAlive: 300000,                // TCP Keep Alive. Needed to prevent proxies from dropping the connection
        connectTimeoutMS: 30000
    })
.then(() => {
    logger.info("Database connection obtained.")
    let db = mongoose.connection;

    db.on('error', (err) => { logger.error("MongoDB Connection failed: " + err); });
    db.on('open', () => { logger.info("Connected to Mongo"); })
    
    const postsRoute = require('./routes/posts');
    const usersRoute = require('./routes/users');
    const opsRoute = require('./routes/ops');
    const notificationsRoute = require('./routes/notifications');
    const imagesRoute = require('./routes/images');
    
    app.use(config.get("www.profilePicUri"), express.static(config.get("www.profilePicUploadDirectory"), { index: false }));
    app.use(bodyParser.json());
    app.use(passport.initialize());
    
    passport.use(User.createStrategy());
    require('./routes/authentication')(app, passport);
    app.use('/api/:version/posts', postsRoute);
    app.use('/api/:version/users', usersRoute);
    app.use('/api/:version/notifications', notificationsRoute);
    app.use('/api/:version/images', imagesRoute);
    // NBTODO: Vulnerability: This is an open API for upload - can be used to kill my server with uploads...
    app.use('/api/clear/:version/ops', opsRoute);
    
    // Error handler - must be last.
    app.use((err, req, res, next) => {
        if (err.name === 'UnauthorizedError') {
            res.status(401).json({ success: false, code: 'Unauthorized', error: err.name, message: err.message });
            return;
        }
    
        // NBTODO: Currently logging more than I need. Better use Express's logger but they use a different logger than I do.
        // Will look into it later...
        logger.error("Exception caught", err);
        logger.error(req.method + ' ' + req.originalUrl);
        logger.error("user: ", req.user? req.user.id : "unknown");
        req.body && req.body.password && (req.body.password = "********");
        logger.error("body: ", JSON.stringify(req.body));
        res.status(500).json({ success: false, code: 'UnhandledException', error: err.message });
    });
    
    const port = config.get('www.listenPort');
    let server;

    if (config.get("security.enableHttps")) {
        const { privateKey, certificate } = config.get("security");
        
        try {
            const options = {
                key: fs.readFileSync(privateKey),
                cert: fs.readFileSync(certificate),
                requestCert: false // Currently we don't authenticate clients.
            };
        
            server = https.createServer(options, app);
        
        } catch (err) {
            logger.error("Could not start secure server.", err);
            throw err;
        }
    }
    else {
        try {
            logger.warn("============================================");
            logger.warn("     Starting unsecure HTTP server");
            logger.warn("           USE FOR DEBUG ONLY");
            logger.warn("============================================");
            server = http.createServer(app);
        }
        catch(error) {
            logger.error("Could not start server.", err);
        }
    }

    server.listen(port);
    server.on('error', (error) => { logger.error("HTTP/S Server error: ", error) });
    server.on('listening', () => { logger.info("Listening on port " + port); });
});