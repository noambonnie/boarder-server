const router = require('express').Router();
const multipartyMiddleware = require('connect-multiparty')();
const fs = require('fs');
const config = require('config');
const UserData = require('../data/users');
const logger = require('winston');
const accountValidators = require('./validators/accountValidators');
const s3 = new (require('aws-sdk')).S3();

// NBTODO: maxFilesSize
// NBTODO: Delete old picture when a new one is uploaded.
// NBTODO: Upload should be to S3
router.post('/setProfilePic', multipartyMiddleware, async (req, res, next) => {
    try {
        const fileExtension = req.files.file.path.substr(req.files.file.path.lastIndexOf("."));
        const destFileName = req.user.id + '-' + Date.now() + fileExtension;
    
        const fileReadStream = fs.createReadStream(req.files.file.path);
        const data = await s3.upload({
            Bucket: config.get('thirdparty.aws.userImagesBucket'),
            Key: destFileName,
            Body: fileReadStream,
            ACL: "public-read",
        }).promise();
        
        const result = await UserData.updateUser(req.user.id, { profilePic: data.Location })
        
        if (!result) {
            // Shouldn't happen because authentication validates user existence.
            return res.status(500).json({ success: false, error: "User not found." });
        }

        res.status(200).json({ success: true, userProfilePic: data.Location });    
    }
    catch(err) {
        next(err);
    }
});

// Update profile
router.put('/userDetails', async (req, res, next) => {
    try {
        await accountValidators.validateUpdateProfile(req.body);
        const newUserData = await UserData.updateUser(req.user.id, req.body);
        res.json({ success: true, userDetails: newUserData });
    }
    catch (error) {
        next(error);
    }
})

router.get('/ownDetails', async (req, res, next) => {
    try {
        const userData = await UserData.getUserOwnData(req.user.id);
        res.json({ success: true, userDetails: userData });
    }
    catch (error) {
        next(error);
    }
})

router.get('/userDetails/:userId', async (req, res, next) => {
    try {
        const userData = await UserData.getUserPublicData(req.params.userId);
        res.json({ success: true, userDetails: userData });
    }
    catch (error) {
        next(error);
    }
})

router.get('/blockUser/:blockedUserId', async (req, res, next) => {
    try {
        if (req.params.blockedUserId === req.user.id) {
            throw new Error("Cannot block own user");
        }

        await UserData.blockUser(req.user.id, req.params.blockedUserId);
        await UserData.setBlockedBy(req.user.id, req.params.blockedUserId);
        
        res.json({ success: true })
    }
    catch (error) {
        next(error)
    }
})

module.exports = router;