const router = require('express').Router();
const multipartyMiddleware = require('connect-multiparty')();
const fs = require('fs');
const config = require('config');
const UserData = require('../data/users');
const logger = require('winston');


// NBTODO: maxFilesSize
router.post('/uploadLog', multipartyMiddleware, (req, res, next) => {
    const destFileName = req.files.file.name;
    const localFileLocation = config.get('www.logsUploadDirectory') + '/' + destFileName;

    fs.copyFile(req.files.file.path,
        localFileLocation,
        (err) => {
            if (err) return next(err);

            res.status(200).json({ success: true });

            fs.unlink(req.files.file.path,
                (err) => {
                    if (err) {
                        // Not failing the request. Just issuing a warning...
                        logger.warn(`Failed to delete file ${req.files.file.path} after log file upload`);
                    }
                })
        })
});

router.get('/heartbeat', (req, res, next) => {
    // NBTODO: Check DB connectivity (and other health checks)
    res.status(200).json({ success: true });
})

module.exports = router;