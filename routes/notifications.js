const logger = require('winston');
const router = require('express').Router();
const notificationsData = require('../data/notifications');
const notificationsFlow = require('../flow/notifications');

router.get('/', async (req, res, next) => {
    try {
        const notifications = await notificationsData.getNotifications(req.user.id, req.query.page, req.query.items, req.query.startDate);
        res.json({ success: true, notifications, });
    }
    catch(error) {
        next(error);
    } 
});

router.get('/count', async (req, res,next) => {
    try {
        const count = await notificationsData.getNotificationsCount(req.user.id);
        res.json({ success: true, count, });
    }
    catch(error) {
        next(error);
    } 
})

router.put('/markRead/:notificationId', async (req, res, next) => {
    try {
        const result = await notificationsFlow.markRead(req.user.id, req.params.notificationId);
        if (result)
            res.json({ success: true });
        else
            res.status(500).json({ success: false, error: "No such notification" });
    }
    catch(error) {
        next(error);
    } 
});

router.put('/markAllSeen', async (req, res, next) => {
    try {
        await notificationsFlow.markAllSeen(req.user.id);
        res.json({ success: true });
    }
    catch(error) {
        next(error);
    } 
});

module.exports = router;
