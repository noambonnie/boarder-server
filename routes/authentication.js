'use strict';

const User = require('../data/db/User');
const userData = require('../data/users');
const clientData = require('../data/clients');
const logger = require('winston');
const jwt = require('jsonwebtoken');
const expressJwt = require('express-jwt');
const crypto = require('crypto'); // To generate a random refresh token
const config = require('config');

// NBTODO: Read from file.
const TOKEN_SECRET = "my FaTher was 4 HamsteR and My Mother sMelt 0f ElderbErri3s fa;skdinufhopwaieuvtbq"
const TOKEN_EXPIRATION = config.get("security.accessTokenExpirationSeconds");
const accountValidators = require('./validators/accountValidators');
const googleAuth = require("../flow/googleAuth");
const facebookAuth = require("../flow/facebookAuth");

// NBTODO: Confirm correct email before activating user (send activation link)
//         https://blog.nodejitsu.com/sending-emails-in-node/ - But look for something that can send HTML emails.
//         Not sure this one can.

let generateAccessToken = function (req, res, next) {
    req.token = req.token || {};
    req.token.accessToken = jwt.sign({
        id: req.user.id,
        clientId: req.user.clientId,
    }, TOKEN_SECRET, {
            expiresIn: TOKEN_EXPIRATION
        });
    next();
}

const authRespond = function (req, res) {
    // NBTODO: Maybe pass non-essential auth info to a separate
    //         getUserDetails request.
    let { id, firstName, lastName, profilePic } = req.user;
    res.status(200).json({
        success: true,
        tokens: req.token,
        user: { id, firstName, lastName, profilePic },
    });
}

const getClientInfo = async (req, res, next) => {
    try {
        await accountValidators.validateClientInfo(req);
        req.client = req.body.client;
        next();
    }
    catch (err) {
        next(err);
    }
}

// NBTODO: Clear expired clients from the database (expired - clients where token has not been refreshed for a while)
//          "a background worker does this every 24 hours with tokens that where inactive for a while"
let serializeClient = async (req, res, next) => {
    try {
        // Just to be safe, delete any client that uses the same notification token. This can only happen if for some
        // reason Logout failed to delete the client. But just to be safe, we're doing it here as well.
        // NBTODO: Index on notificationToken.
        // NBTODO: Set each client's last active time so I can delete inactive clients.
        await clientData.removeClientsByNotificationToken(req.client.notificationToken)
        const client = await clientData.createClient(req.user.id, req.client);
        req.user.clientId = client.id;
        next();
    }
    catch (err) {
        return next(err);
    }
}

let generateRefreshToken = async (req, res, next) => {
    try {
        // NBTODO: I believe this is not implemented correctly. If refresh token is compromised, no problem to get new access token
        //         and even invalidate existing client's refresh token (because we change the refresh token upon refresh of access token)

        // Generate a refresh token - a random number...
        req.token.refreshToken = req.user.clientId + '.' + crypto.randomBytes(40).toString('hex');

        // Store the refresh token for the client.
        await clientData.setRefreshToken(req.user.clientId, req.token.refreshToken);
        next();
    }
    catch (err) {
        next(err);
    }
}

let validateRefreshToken = async (req, res, next) => {
    try {
        // Refresh Token is <clientId>.<token> format. Delete by Client ID.
        const clientId = req.body.refreshToken.split('.')[0];
        const client = await clientData.findById(clientId);

        // NBTODO: Delete the refresh token in this case and logout. It might be that an attacker used a previous refresh token.
        //         http://www.ietf.org/mail-archive/web/oauth/current/msg02292.html
        if (!client || client.refreshToken !== req.body.refreshToken) {
            return next(new Error("InvalidRefreshToken"));
        }

        // Next middleware is gonna use this to generate a new access token and a new refresh token.
        req.user = req.user || {};
        req.user.id = client.userId;
        req.user.clientId = client.id;
        next();
    }
    catch (err) {
        next(err);
    }
}

const logUserActivity = (req, res, next) => {
    // We don't want to delay anything for logging. Pass the control to the next middleware and then act on the request.
    next();
    req.user && req.user.id && userData.updateUserLastActivity(req.user.id);
    req.user && req.user.clientId && clientData.updateClientLastActivity(req.user.clientId);
}

module.exports = function (app, passport) {

    // Don't check token for any clear APIs. Those will either be public or will require username/password
    app.use('/api', expressJwt({ secret: TOKEN_SECRET }).unless({ path: [RegExp('\/api\/clear.*')] }));
    app.use('/api', logUserActivity);

    // NBTODO: Change route names to something consistent and under "auth" route

    // New user registration
    app.post('/api/clear/:version/register', getClientInfo, (req, res, next) => {

        accountValidators.validateRegisterUser(req.body)
            .then(() => {
                req.body.email = req.body.email.toLowerCase().trim();
                req.body.firstName = req.body.firstName.trim();
                req.body.lastName = req.body.lastName.trim();
                User.register(req.body, req.body.password, function (error, user) {
                    if (error) {
                        res.json({ success: false, error: error });
                    }
                    else {
                        // Successfully registered user. Set user data to the request object so we can generate a token.
                        req.user = user;
                        next();
                    }
                })
            })
            .catch((error) => {
                next(error);
            });
    }, serializeClient, generateAccessToken, generateRefreshToken, authRespond);

    app.get('/api/clear/:version/emailAvailable/:email', function (req, res, next) {

        User.findOne({ email: req.params.email })
            .then((result) => {
                res.status(200).json({ success: true, available: (!result) });
            })
    });

    app.post('/api/clear/:version/googleLogin', getClientInfo,
        async (req, res, next) => {
            try {
                req.user = await googleAuth.loginOrRegister(req.body.idToken);
                next();
            }
            catch (error) {
                res.status(401).json({ success: false, code: error, message: error.message });
            }
        },
        serializeClient,
        generateAccessToken,
        generateRefreshToken,
        authRespond);

    app.post('/api/clear/:version/facebookLogin', getClientInfo,
        async (req, res, next) => {
            try {
                req.user = await facebookAuth.loginOrRegister(req.body.accessToken);
                next();
            }
            catch (error) {
                res.status(401).json({ success: false, code: error, message: error.message });
            }
        },
        serializeClient,
        generateAccessToken,
        generateRefreshToken,
        authRespond);

    app.post('/api/clear/:version/login', getClientInfo, (req, res, next) => {
        passport.authenticate(
            'local',                            // Local passport strategy
            { session: false },                 // Don't care about sessions it's an API.
            (err, user, response) => {          // Callback is needed so we can return a JSON with message
                if (err) { return next(err); }  // in case authentication failed.

                if (!user) {
                    return res.status(401).json({ success: false, code: response.name, message: response.message });
                }

                // NBTODO: Isn't there a nicer way to do this? With promises and stuff instead of this oldschool
                //         callback mess?
                req.user = user;
                next();
            })(req, res, next)
    },
        serializeClient,
        generateAccessToken,
        generateRefreshToken,
        authRespond);

    app.post('/api/clear/:version/logout', async (req, res) => {
        // NBTODO: Look at isRevoked callback https://github.com/auth0/express-jwt for revoking the access token as well.

        // Refresh Token is <clientId>.<token> format. Delete by Client ID.
        if (req.body.refreshToken) {
            let clientId = req.body.refreshToken.split('.')[0]
            try {
                await clientData.removeById(clientId)
            }
            catch (err) {
                logger.warn(`Logout failed to remove client ${clientId}: ${err}`);
            }

            // Return logout success even if deleting the client failed.
            res.json({ success: true });
        }
    });

    app.post('/api/clear/:version/refreshToken',
        validateRefreshToken,
        generateAccessToken,
        generateRefreshToken,
        authRespond);

    app.post('/api/clear/:version/forgot', function (req, res) {

        User.generateResetToken(req.body.email, function (err, user) {
            if (err) {
                // NBTODO: Throughout the file, change the error message to send the error passed to the callback.
                res.json({ success: false, error: 'Issue finding user.' });
            } else {
                let token = user.resetToken;
                // NBTODO: Read address from config...
                let resetLink = 'http://localhost:3000/reset/' + token + '/' + user.email;

                // NBTODO: This is not secure. Need to setup a mail client and send a message with the link in it.
                // NBTODO: Until this is done, comment out the /forgot/ functionality.
                res.json({ success: true, resetLink: resetLink });
            }
        });
    });

    app.get('/api/clear/:version/reset/:token/:email', function (req, res) {
        let token = req.params.token; // NBTODO: Wouldn't it be better to use JWT token that has expiration and email in it?
        let email = req.params.email;

        if (!token) {
            res.json({ success: false, error: 'Request missing a reset token.' });
        }
        else {
            // NBTODO: How do we reset a password? Simply let the user modify along with the reset token to make sure
            //         it's still the same person?
            //         I think this should be an API that gets the reset token and a new password and upon token validation
            //         sets the new password.
            //
            //1. find user with reset_token == token .. no match THEN error
            //2. check now.getTime() < reset_link_expires_millis
            //3. if not expired, present reset password page/form
            res.render('resetpass', { email: email });
        }
    });

    app.get('/api/:version/setNotificationToken/:token', async (req, res) => {
        await clientData.setNotificationToken(req.user.clientId, req.params.token);
        res.json({ success: true });
    })

    // NBTODO: Validations. Currently allows short passwords.
    // NBTODO: API is broken. "findUserByEmailOnly" is not a function.
    app.post('/api/:version/resetPassword', validateRefreshToken, function (req, res) {

        let email = req.body.email;
        let currentPassword = req.body.currentPassword;
        let newPassword = req.body.newPassword;
        let confirmationPassword = req.body.confirmationPassword;

        // NBTODO: Invalidate token after change password?
        if (email && currentPassword && newPassword && confirmationPassword && (newPassword === confirmationPassword)) {

            User.findUserByEmailOnly(email, function (err, user) {
                if (err) {
                    res.json({ success: false, error: 'Issue while finding user.' });
                }
                else if (!user) {
                    res.json({ success: false, error: 'Unknown user email: ' + email });
                }
                else if (user) {
                    User.authenticate()(email, currentPassword, function (err, isMatch, options) {
                        if (err) {
                            res.json({ success: false, error: 'Error while verifying current password.' });
                        }
                        else if (!isMatch) {
                            res.json({ success: false, error: 'Current password does not match' });
                        }
                        else {
                            user.setPassword(newPassword, function (err, user) {
                                if (err) {
                                    res.json({ success: false, error: 'Issue while setting new password.' });
                                }
                                user.save(function (err, usr) { // NBTODO: usr is not used.
                                    if (err) {
                                        // cb(err, null); NBTODO: CB not defined. I added the response instead.
                                        res.json({ success: false, error: "Save user failed." })
                                    } else {
                                        //TODO, client will redirect to Login page (they won't have a current token)
                                        res.json({ success: true });
                                    }
                                });
                            });
                        }
                    });
                }
            });
        }
        else {
            //TODO Better error message,etc.
            res.json({ success: false, error: 'Missing email, current, new, or confirmation password, OR, the confirmation does not match.' });
        }
    });
};
