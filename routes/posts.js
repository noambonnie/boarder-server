const logger = require('winston');
const router = require('express').Router();
const postData = require('../data/posts');
const userData = require('../data/users');
const postValidators = require('./validators/postValidators');
const notifications = require('../flow/notifications');
const config = require('config');
const s3 = new (require('aws-sdk')).S3();

// NBTODO: Add automated tests.

const copyImages = async (images, fromBucket, fromFolder, toBucket, toFolder) => {
    const imagePromises = images && images.map((image) =>
        new Promise(async (resolve, reject) => {
            s3.copyObject({
                Bucket: toBucket,
                CopySource: `/${fromBucket}/${fromFolder}/${image}`,
                Key: `${toFolder}/${image}`,
                ACL: 'public-read',
            },
                (err, data) => {
                    if (err) {
                        logger.error(`Error copying S3 /${fromBucket}/${fromFolder}/${image} to /${toBucket}/${toFolder}/${image}`, err);
                        return reject(err);
                    }
                    resolve();
                })
        })
    ) || [];

    return Promise.all(imagePromises);
}

router.post('/', async (req, res, next) => {
    try {
        await postValidators.validateAddPost(req.body);
        const uploadFolder = config.get("thirdparty.aws.postImageUploadFolder");
        const uploadPrefixLength = uploadFolder.length + 1; // +1 for the '/'
        const unprefixedImages = req.body.images && req.body.images.map((image) => image.substr(uploadPrefixLength));
        await copyImages(
            unprefixedImages,
            config.get('thirdparty.aws.postImagesBucket'),
            uploadFolder,
            config.get('thirdparty.aws.postImagesBucket'),
            config.get("thirdparty.aws.postImageOriginalsFolder")
        )

        const post = await postData.addPost(req.user.id, { ...req.body, images: unprefixedImages });
        res.json({ success: true, post: post });
    }
    catch (error) {
        next(error);
    }
});

router.get('/:id', async (req, res, next) => {
    try {
        const post = await postData.getPostById(req.params.id);
        res.json({
            success: true,
            post: {
                ...post,
                originalImagesPrefix: config.get("thirdparty.aws.postImageOriginalsFolder"),
                imageResizerPrefixURL: config.get("thirdparty.aws.imageResizerPrefixURL")
            }
        });
    }
    catch (err) {
        return next(err);
    }
});

router.put('/:id', function (req, res, next) {
    res.status(501).json({ success: false, code: "Unsupported", error: "The API is not implemented" });
    // Post.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
    //     if (err) return next(err);
    //     res.json(post);
    // });
});

router.delete('/:id', function (req, res, next) {
    res.status(501).json({ success: false, code: "Unsupported", error: "The API is not implemented" });
    // Post.findByIdAndRemove(req.params.id, req.body, function (err, post) {
    //     if (err) return next(err);
    //     res.json(post);
    // });
});

router.post('/feed/', async (req, res, next) => {
    try {
        const queryRestrictions = await userData.getUserQueryRestrictions(req.user.id);
        const posts = await postData.getFeed(req.body, req.query.page, req.query.items, req.query.startDate, queryRestrictions)
        res.json({ success: true, now: Date.now(), posts: posts, })
    }
    catch (error) {
        next(error);
    };
});

// ----------------------------------------------------------------------
//  Comments for posts are technically post objects
// ----------------------------------------------------------------------

// Add comment to post
router.post('/comment/:postId', function (req, res, next) {
    postValidators.validateAddComment(req.params.postId, req.body)
        .then(() => postData.addComment(req.params.postId, req.user.id, req.body))
        .then(() => postData.getComments(req.params.postId))
        .then((comments) => {
            res.json({ success: true, comments: comments, });
            return comments;
        })
        .then((comments) => {
            notifications.addPostCommentNotification(req.user.id, req.params.postId, comments, req.body.text);
        })
        .catch((error) => {
            next(error);
        });
});

// Get post comments
router.get('/comment/:postId', async (req, res, next) => {
    try {
        const queryRestrictions = await userData.getUserQueryRestrictions(req.user.id);
        const comments = await postData.getComments(req.params.postId, queryRestrictions)
        res.json({ success: true, comments: comments, });
    }
    catch (error) {
        next(error);
    }
});

router.put('/comment/:id', function (req, res, next) {
    res.status(501).json({ success: false, code: "Unsupported", error: "The API is not implemented" });

    // Post.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
    //     if (err) return next(err);
    //     res.json(post);
    // });
});

router.delete('/comment/:id', function (req, res, next) {
    res.status(501).json({ success: false, code: "Unsupported", error: "The API is not implemented" });
    // Post.findByIdAndRemove(req.params.id, req.body, function (err, post) {
    //     if (err) return next(err);
    //     res.json(post);
    // });
});

module.exports = router;
