const router = require('express').Router();
const config = require('config');
const s3 = new (require('aws-sdk')).S3();
const uuid = require('uuid/v1'); // timestamp based UUID

const bucketNames = {
    "profilePics": config.get('thirdparty.aws.userImagesBucket'),
    "postImages": config.get('thirdparty.aws.postImagesBucket'),
}

router.get('/uploadUrl/:bucket', async (req, res, next) => {
    try {
        if (!req.query['contentType']) throw(new Error(`Parameter contentType required`));
        
        const bucketName = bucketNames[req.params.bucket];
        if (!bucketName) throw(new Error(`Invalid bucket name '${req.params.bucket}'`));

        const key = `${config.get("thirdparty.aws.postImageUploadFolder")}/${req.user.id}_${uuid().toString()}`;

        s3.getSignedUrl(
            "putObject", 
            {
                ACL: 'private',
                Bucket: bucketName,
                Key: key,
                Expires: config.get("thirdparty.aws.imageUploadExpirationSeconds"),
                ContentType: req.query['contentType'],
                // Tagging: "pendingUpload=true" Tagging doesn't work for presigned URL. Resorting to prefix for now.
            },
            (err, url) => {
                err && next(err);
                res.json({ success: true, url, key });
            }
        )
    }
    catch (err) {
        return next(err);
    }
});

module.exports = router;
