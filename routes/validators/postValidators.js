const postData = require('../../data/posts');

const validateGeolocation = (geolocation) => {
    return new Promise(
        function (resolve, reject) {
            if (!geolocation.position ||
                !geolocation.position.latitude ||
                !geolocation.position.longitude) {
                return reject(new Error('Invalid geolocation'));
            }
            resolve();
        })
}

module.exports.validateAddPost = (reqData) => {
    return new Promise(
        function (resolve, reject) {
            if (!reqData.title ||
                !reqData.text ||
                !reqData.geolocation) {
                return reject(new Error("Missing field"));
            }

            resolve(validateGeolocation(reqData.geolocation));
        })
}

module.exports.validateAddComment = (postId, reqData) => {

    // NBTODO: Make sure requests only contain valid fields.`
    return new Promise(
        function (resolve, reject) {
            if (!postId ||
                !reqData.text) {
                return reject(new Error("Missing field"));
            }

            // NBTODO: This is wrong. resolving with a promise is not the way to go
            //         because validation was not done and therefore should not be resolved.
            postData.postCountById(postId)
                .then((result) => {
                    if (result !== 1)
                        reject(new Error("No such parent post."));
                })
                .then(() => resolve())
                .catch((error) => {
                    reject(error);
                })
        })

}
