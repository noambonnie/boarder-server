const validator = require('validator');
// NBTODO: There must be a nicer way to do this than those long if's.
module.exports.validateRegisterUser = (reqData) => {
    return new Promise(
        function (resolve, reject) {
            if (
                !reqData.password.trim()
                || !reqData.firstName.trim()
                || !reqData.lastName.trim()
                || !reqData.email.trim()
            ) 
            {
                return reject(new Error("Missing field"));
            }

            if (reqData.password.length < 8) {
                return reject(new Error("Password too short"));
            }
            
            if (!validator.isEmail(reqData.email.trim())) {
                return reject(new Error("Invalid email address"));
            }

            resolve();
        })
}

// NBTODO: This function is incosistent with other validators, but maybe better. This is for middleware use.
module.exports.validateClientInfo = (req) => {
    const reqData = req.body;
    return new Promise(
        function (resolve, reject) {
            if (
                   !reqData.client
                || !reqData.client.os 
                || !reqData.client.os.name 
                || !reqData.client.os.version 
            ) 
            {
                return reject(new Error("Missing client field"));
            }

            resolve();
        })
}

module.exports.validateUpdateProfile = (reqData) => {
    return new Promise(
        function (resolve, reject) {
            if (!reqData.firstName.trim() || reqData.firstName.trim() == null ||
                !reqData.lastName.trim() || reqData.lastName.trim() == null) {
                return reject(new Error("Missing field"));
            }
            resolve();
        })
}